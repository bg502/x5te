FROM python

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN chmod -R 777 *

RUN pip3 install requests 

EXPOSE 8002

cmd ["python3", "server.py"]
